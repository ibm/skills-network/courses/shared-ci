import os.path
from os import path
import shutil
import yaml
import csv_to_sqlite
import sqlite3


def generateSQLiteDatabase():
    if not path.exists('./datasets/dataset.yml'):
        print("No dataset found")
        return

    f = open('./datasets/dataset.yml', 'r')
    courseYAML = yaml.load(f.read())

    for filename, name in courseYAML['datasets'].items():
        shutil.copy('datasets/' + filename, '/tmp/'+name+'.csv')

    csvfiles = list(map(lambda f: "/tmp/" + f + ".csv", courseYAML['datasets'].values()))
    outputFilename = "./datasets/" + courseYAML['name'] + ".db"
    options = csv_to_sqlite.CsvOptions(drop_tables=True)
    csv_to_sqlite.write_csv(csvfiles, outputFilename, options)

    if 'saved_queries' in courseYAML:
        # Connecting to the database file
        conn = sqlite3.connect(outputFilename)
        c = conn.cursor()

        c.execute('CREATE TABLE saved_queries (name string PRIMARY KEY, sql string, author_id string)')

        for name, sql in courseYAML['saved_queries'].items():
            c.execute("insert into saved_queries (name, sql, author_id) values ('" + name + "', '" + sql + "', '')")

        conn.commit()
        conn.close()

    return

try:
    generateSQLiteDatabase()
except:
    print("Failed to write sqlite Database")
    exit(1)
