import os
import shutil
import glob
import datetime
import json

def isoformat(dt):
    """a small helper to user ``Z`` for UTC ISO strings"""
    return dt.isoformat().replace("+00:00", "Z")

def fileModified(path):
    dt = datetime.datetime.utcfromtimestamp(os.path.getmtime(path))
    return isoformat(dt)

def fileCreated(path):
    dt = datetime.datetime.utcfromtimestamp(os.path.getctime(path))
    return isoformat(dt)

def create_index(path, base_path = 'jupyterlite/files'):
    full_path = os.path.join(base_path, path)
    print(full_path)
    print(path)
    if os.path.isdir(full_path):
        return {
            "content": None,
            "created": fileCreated(full_path),
            "format": "json",
            "last_modified": fileModified(full_path),
            "mimetype": None,
            "name": os.path.basename(path),
            "path": path,
            "size": None,
            "type": "directory",
            "writable": True
        }
    else:
        return {
            "content": None,
            "created": fileCreated(full_path),
            "format": None,
            "last_modified": fileModified(full_path),
            "mimetype": None,
            "name": os.path.basename(path),
            "path": path,
            "size": os.path.getsize(full_path),
            "type": "notebook",
            "writable": True
        }

def get_contents():
    return glob.glob('labs/**/*.jupyterlite.ipynb', recursive=True)


def copy_to_jupyterlite(src):
    if src.startswith('labs/'):
        dst = os.path.join('jupyterlite', 'files', src[5:])
    else:
        dst = os.path.join('jupyterlite', 'files', src)
    
    dstfolder = os.path.dirname(dst)
    print(dstfolder)
    if not os.path.exists(dstfolder):
        os.makedirs(dstfolder)
    shutil.copy(src, dst)



contents = get_contents()

for file in contents:
    copy_to_jupyterlite(file)

for (root,dirs,files) in os.walk('jupyterlite/files', topdown=True):
    path = root[18:]
    all = create_index(path)
    all['content'] = []
    for dir in dirs:
        all['content'].append(create_index(os.path.join(path, dir)))
    for file in files:
        all['content'].append(create_index(os.path.join(path, file)))
    content_folder = os.path.join('jupyterlite', 'api', 'contents', root[18:])
    if not os.path.exists(content_folder):
        os.makedirs(content_folder)
    with open(os.path.join(content_folder, "all.json"), "w") as f:
        f.write(json.dumps(all, indent = 2))
