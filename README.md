# shared-ci

Skills Network syndicates/distributes its content from the [SN Faculty Object Sorage instance](
https://cloud.ibm.com/objectstorage/crn%3Av1%3Abluemix%3Apublic%3Acloud-object-storage%3Aglobal%3Aa%2Ff13f7985d6f4e8621dc8a18df501122f%3A381b7705-35e2-4121-aeea-dfc33148875f%3A%3A?paneId=manage) of the IBM Cloud Object Storage

The content is stored in the bucket called cf-courses-data.

This shared CI/CD pipeline helps course authors distribute their course content by moving content from the master branch of the course "Repository" to the cf-courses-data bucket. 

Any time a file is checked in to the **Master** branch of specific course repository It will:

- Output files to be uploaded to IBM Cloud Object storage along with their path on branches (except master) and pull requests
- Upload the files once merged into master
- Create a file directory and publish via GitLab Pages

The pipeline will __NOT__ delete files in the cf-courses-data but __files will be overwritten if the same key already exists in the bucket__

## Getting Started

To take advantage of this CI/CD pipeline, course author has to:
1. Create a [new project/repository for your course](https://gitlab.com/projects/new?namespace_id=6197590). Make sure to use your course number as the project name and provide course title in the description.

1. Add a new file with the name `.gitlab-ci.yml` with the following content:

```yml
include:
- project: 'ibm-skills-network/courses/shared-ci'
  ref: master
  file: 'shared-ci.yml'
```
1. Upload all of the files for the course content to the master branch

🎉🎉🎉 You're done! 🎉🎉🎉

## File Directory

Course author can use the auto-published file directory via the project GitLab Pages at `https://ibm-skills-network.gitlab.io/courses/<course_number>`. For example, [ibm-skills-network.gitlab.io/courses/test101/](https://ibm-skills-network.gitlab.io/courses/test101/). The file directory enables course author to easily browse through the uploaded files and copy the public URLs.



## Additional configuration

The shared ci pipeline will work out of the box but we can also override the default functionality if required. The following variables can be defined to override the defaults on your projects page under `Settings > CI / CD > Variables`.

- __`AWS_ACCESS_KEY_ID`__: __Required__. Provided [HMAC credentials](https://cloud.ibm.com/docs/cloud-object-storage?topic=cloud-object-storage-uhc-hmac-credentials-main).
- __`AWS_SECRET_ACCESS_KEY`__: __Required__. Provided [HMAC credentials](https://cloud.ibm.com/docs/cloud-object-storage?topic=cloud-object-storage-uhc-hmac-credentials-main).
- __`AWS_DEFAULT_REGION`__: __Required__. Region of the bucket.
- __`IBM_COS_BUCKET`__: Default is `cf-courses-data`
- __`IBM_COS_UPLOAD_PREFIX`__: Default is empty string, you can use this to add a prefix to all uploads in the project. __NOTE__: The value should end with a forward slash `/`.
