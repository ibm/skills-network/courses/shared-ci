FROM ubuntu:18.04

# Install system packages and sentry
RUN apt-get update && \
    apt-get install -y \
    curl unzip python3-pip wget rsync jq moreutils && \
    rm -rf /var/lib/apt/lists/* && \
    curl -sL https://sentry.io/get-cli/ | bash

# Install csv-to-sqlite
RUN pip3 install csv-to-sqlite==2.1.1 pyyaml==5.3.1

COPY sqlite_generator.py /sqlite_generator.py
COPY build_api_contents.py /build_api_contents.py
COPY fixipynb.sh /fixipynb.sh

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# Install AWS CLI
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    rm -rf aws awscliv2.zip

ENV LINKIFY_VERSION=2.1.6

RUN curl -o /bin/linkify https://skills-network-assets.s3.us.cloud-object-storage.appdomain.cloud/cli/linkify-${LINKIFY_VERSION}-linux && \
    chmod +x /bin/linkify
